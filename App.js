import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ProgressBar} from './src/components/ProgressBar';

export default function App() {
  const [number, setNumber] = useState(0);
  const [pause, setPause] = useState(false);
  const [start, setStart] = useState(false);
  useEffect(() => {
    if (start) {
      const intervalId = setInterval(() => {
        if (number !== 15) {
          setNumber(number + 1);
        } else {
          clearInterval(intervalId);
        }
      }, 500);
      if (pause) {
        clearInterval(intervalId);
      }
      return () => clearInterval(intervalId);
    }
  });
  return (
    <SafeAreaView>
      <StatusBar />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <Text style={styles.title}>Progress Bar</Text>
        <ProgressBar number={number} />
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => (pause === true ? setPause(false) : setStart(true))}>
            <View style={styles.button}>
              <Text>Start</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setPause(true)}>
            <View style={styles.button}>
              <Text>Stop</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setPause(true);
              setNumber(0);
            }}>
            <View style={styles.button}>
              <Text>Reset</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 10,
  },
  box: {
    borderWidth: 5,
    height: 20,
    width: 20,
  },
  separator: {
    width: 7,
  },
  button: {
    width: 50,
    borderWidth: 1,
    elevation: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 25,
    fontWeight: '800',
  },
});
