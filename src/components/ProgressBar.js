import React from 'react';
import {StyleSheet, View} from 'react-native';
export const ProgressBar = ({number}) => {
  return (
    <View style={styles.row}>
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 1 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 2 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 3 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 4 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 5 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 6 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 7 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 8 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 9 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 10 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 11 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 12 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 13 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 14 ? '#FF00ac' : '#FFF',
        }}
      />
      <View style={styles.separator} />
      <View
        style={{
          ...styles.box,
          backgroundColor: number >= 15 ? '#FF00ac' : '#FFF',
        }}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 30,
  },
  box: {
    borderWidth: 5,
    height: 20,
    width: 20,
  },
  separator: {
    width: 7,
  },
});
